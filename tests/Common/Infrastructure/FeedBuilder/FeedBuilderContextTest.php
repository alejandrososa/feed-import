<?php
namespace App\Tests\Common\Infrastructure\FeedBuilder;

use App\Common\Infrastructure\FeedBuilder\FeedBuilderContext;
use App\Common\Infrastructure\FeedBuilder\FeedBuilderStrategy;
use App\Common\Infrastructure\FeedBuilder\FeedBuilderStrategyNotFound;
use App\Common\Infrastructure\FeedBuilder\FeedFactory;
use App\Tests\Shared\PHPUnit\UnitTestCase;
use PHPUnit\Framework\MockObject\MockObject;
use Zend\Feed\Reader\Feed\FeedInterface;

class FeedBuilderContextTest extends UnitTestCase
{
    /**
     * @var MockObject|FeedFactory
     */
    private $factory;
    /**
     * @var MockObject|FeedInterface
     */
    private $feed;

    protected function setUp(): void
    {
        $this->factory = $this->mock(FeedFactory::class);
        $this->feed = $this->mock(FeedInterface::class);
    }

    protected function tearDown(): void
    {
        $this->factory = null;
        $this->feed = null;
    }

    public function test_throw_exception_if_type_is_not_declarated()
    {
        $this->expectException(FeedBuilderStrategyNotFound::class);
        $senderContext = new FeedBuilderContext($this->factory);
        $senderContext->build($this->feed);
    }

    private function getStrategies()
    {
        $strategyOne = $this->mock(FeedBuilderStrategy::class);
        $strategyOne->expects($this->once())->method('match')->willReturn(false);
        $strategyOne->expects($this->never())->method('build')->willReturn(null);

        $strategyTwo = $this->mock(FeedBuilderStrategy::class);
        $strategyTwo->expects($this->once())->method('match')->willReturn(true);
        $strategyTwo->expects($this->atLeastOnce())->method('build')->willReturn(null);

        yield $strategyOne;
        yield $strategyTwo;
    }

    public function test_send_specific_feed_by_strategy_match()
    {
        $strategies = $this->getStrategies();
        $this->factory->method('strategies')->willReturn($strategies);

        $senderContext = new FeedBuilderContext($this->factory);
        $senderContext->setType('fake_type');
        $senderContext->build($this->feed);
    }
}
