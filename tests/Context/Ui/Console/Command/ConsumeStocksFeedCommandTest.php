<?php
namespace App\Tests\Context\Ui\Console\Command;

use App\Context\Ui\Console\Command\ConsumeStocksFeedCommand;
use App\Tests\Shared\PHPUnit\IntegrationTestCase;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class ConsumeStocksFeedCommandTest extends IntegrationTestCase
{
    /**
     * @var ContainerInterface|MockObject
     */
    private $containerServices;
    /**
     * @var MessageBusInterface|MockObject
     */
    private $bus;
    private $command;
    private $commandTester;
    private $application;
    private $pathResources;

    protected function setUp()
    {
        $this->bus = $this->createMock(MessageBusInterface::class);
        $this->containerServices = $this->createMock(ContainerInterface::class);

        $kernel = static::createKernel();
        $this->application = new Application($kernel);
        $this->application->add(
            new ConsumeStocksFeedCommand(
                $this->bus,
                $this->containerServices
            )
        );

        $this->command = $this->application->find('app:consume-feed-stocks');
        $this->commandTester = new CommandTester($this->command);

        $this->pathResources = $kernel->getProjectDir().'/config/feeds/stock.xml';
    }

    protected function tearDown(): void
    {
        $this->entityManager = null;
        $this->entityManager = null;
        $this->commandTester = null;
        $this->application = null;
    }

    /** @ */
    public function testThrowMessageWhenUrlIsNotValid()
    {
        $this->markTestSkipped( 'PHPUnit will skip this test method' );

        $this->shouldBusDispatch();

        $this->commandTester->execute([
            'command'  => $this->command->getName(),
            'url' => 'urlnovalid',
        ]);

        // the output of the command in the console
        $output = $this->commandTester->getDisplay();
        $this->assertContains("Upss, 'urlnovalid' resource is not valid", $output);
    }

    public function testExecute()
    {
        $this->markTestSkipped( 'PHPUnit will skip this test method' );

        $this->shouldBusDispatch();

        $this->commandTester->execute([
            'command'  => $this->command->getName(),
            'url' => $this->pathResources,
        ]);

        // the output of the command in the console
        $output = $this->commandTester->getDisplay();
        $this->assertContains("[OK] Stocks successfully imported.", $output);
    }
}