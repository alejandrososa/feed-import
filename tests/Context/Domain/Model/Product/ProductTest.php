<?php
namespace App\Tests\Context\Domain\Model\Product;

use App\Context\Domain\Model\Product\Product;
use App\Context\Domain\Model\Product\ProductLink;
use App\Context\Domain\Model\Product\ProductTitle;
use App\Context\Domain\Model\Product\ProductDescription;
use App\Context\Domain\Model\Product\ProductTagImageLink;
use App\Context\Domain\Model\Product\ProductTagPrice;
use App\Context\Domain\Model\Product\ProductTagReducedPrice;
use App\Context\Domain\Model\Product\ProductTagEan;
use App\Context\Domain\Model\Product\ProductTagId;
use App\Tests\Shared\PHPUnit\UnitTestCase;

class ProductTest extends UnitTestCase
{
    /**
     * @expectedException \App\Context\Domain\Exception\InvalidValue
     * @throws \App\Context\Domain\Exception\InvalidValue
     */
    public function testsThrowExceptionIfTheSignatureIsEmpty()
    {
        $produc = Product::create(
            ProductTitle::fromString(''),
            ProductLink::fromString(''),
            ProductDescription::fromString(''),
            ProductTagImageLink::fromString(''),
            ProductTagPrice::fromInt(''),
            ProductTagReducedPrice::fromInt(''),
            ProductTagEan::fromInt(''),
            ProductTagId::fromInt('')
        );
    }

    public function testsCreateProduct()
    {
        $produc = Product::create(
            ProductTitle::fromString($this->fake()->title),
            ProductLink::fromString($this->fake()->url),
            ProductDescription::fromString($this->fake()->paragraph),
            ProductTagImageLink::fromString($this->fake()->imageUrl()),
            ProductTagPrice::fromInt($this->fake()->numberBetween(1,100)),
            ProductTagReducedPrice::fromInt($this->fake()->numberBetween(1,100)),
            ProductTagEan::fromInt($this->fake()->numberBetween(1,1000)),
            ProductTagId::fromInt($this->fake()->numberBetween(1,1000))
        );

        $this->assertInstanceOf(Product::class, $produc);
    }
}
