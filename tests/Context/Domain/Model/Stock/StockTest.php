<?php
namespace App\Tests\Context\Domain\Model\Stock;

use App\Context\Domain\Model\Stock\Stock;
use App\Context\Domain\Model\Stock\StockTagPrice;
use App\Context\Domain\Model\Stock\StockTagReducedPrice;
use App\Context\Domain\Model\Stock\StockTagEan;
use App\Tests\Shared\PHPUnit\UnitTestCase;
use Faker\Factory;
use Faker\Generator;

class StockTest extends UnitTestCase
{
    /**
     * @var Generator
     */
    private $faker;

    protected function setUp()
    {
        $this->faker = Factory::create();
    }

    /**
     * @expectedException \App\Context\Domain\Exception\InvalidValue
     * @throws \App\Context\Domain\Exception\InvalidValue
     */
    public function testsThrowExceptionIfTheSignatureIsEmpty()
    {
        $stock = Stock::create(
            StockTagEan::fromInt(null),
            StockTagPrice::fromInt(null),
            StockTagReducedPrice::fromInt(null)
        );
    }

    public function testsCreateProduct()
    {
        $stock = Stock::create(
            StockTagEan::fromInt($this->faker->numberBetween(1,1000)),
            StockTagPrice::fromInt($this->faker->numberBetween(1,100)),
            StockTagReducedPrice::fromInt($this->faker->numberBetween(1,100))
        );

        $this->assertInstanceOf(Stock::class, $stock);
    }
}
