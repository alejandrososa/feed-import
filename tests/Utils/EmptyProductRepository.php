<?php
namespace App\Tests\Utils;

use App\Context\Domain\Model\Product\Product;
use App\Context\Domain\Model\Product\ProductRepository;
use App\Context\Domain\Model\Product\ProductTagEan;

class EmptyProductRepository implements ProductRepository
{
    public function get(ProductTagEan $ean)
    {
    }

    public function save(Product $product): void
    {
    }
}
