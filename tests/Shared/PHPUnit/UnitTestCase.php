<?php
/**
 * web, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2019, 10/04/2019 16:00
 */

namespace App\Tests\Shared\PHPUnit;

use Faker\Factory;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

abstract class UnitTestCase extends TestCase
{
    private $fake;

    protected function tearDown(): void
    {
        $this->fake = null;
    }

    /** @return \Faker\Generator */
    protected function fake()
    {
        return $this->fake = $this->fake ?: Factory::create();
    }

    protected function mock($className): MockObject
    {
        return $this->createMock($className);
    }
}
