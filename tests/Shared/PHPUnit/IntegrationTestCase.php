<?php
/**
 * web, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2019, 10/04/2019 16:00
 */

namespace App\Tests\Shared\PHPUnit;

use stdClass;
use App\Context\Domain\Model\Product\ProductRepository;
use Faker\Factory;
use Faker\Generator;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

abstract class IntegrationTestCase extends KernelTestCase
{
    private $fake;
    private $eventBus;
    private $productRepository;

    /** @return Generator */
    protected function fake()
    {
        return $this->fake = $this->fake ?: Factory::create();
    }

    protected function mock($className): MockObject
    {
        return $this->createMock($className);
    }

    /** @return MessageBusInterface|MockObject */
    protected function eventBus()
    {
        return $this->eventBus = $this->eventBus ?: $this->mock(MessageBusInterface::class);
    }

    /** @return ProductRepository|MockObject */
    protected function productRepository()
    {
        return $this->productRepository = $this->productRepository ?: $this->mock(ProductRepository::class);
    }

    protected function shouldBusDispatch()
    {
        $this->eventBus()
            ->expects($this->once())
            ->method('dispatch')
            ->willReturn(new Envelope(new stdClass));
    }
}
