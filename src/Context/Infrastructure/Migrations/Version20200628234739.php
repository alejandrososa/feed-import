<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200628234739 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Create products and stocks tables';
    }

    public function up(Schema $schema) : void
    {
        //products
        $tbl = $schema->createTable('products');
        $tbl->addColumn('ean', 'integer', ['notnull'=>true]);
        $tbl->addColumn('title', 'string', ['notnull'=>true]);
        $tbl->addColumn('link', 'string', ['notnull'=>false]);
        $tbl->addColumn('description', 'text', ['notnull'=>false]);
        $tbl->addColumn('tag_price', 'decimal', ['default'=>0]);
        $tbl->addColumn('tag_reduced_price', 'decimal', ['default'=>0]);
        $tbl->addColumn('tag_image_link', 'string', ['notnull'=>true]);
        $tbl->addColumn('tag_id', 'integer', []);

        $tbl->setPrimaryKey(['ean']);
        $tbl->addUniqueIndex(['ean'], 'products_id_uindex');
        $tbl->addIndex(['ean','tag_id'], 'idx_numeric');
        $tbl->addIndex(['tag_price','tag_reduced_price'], 'idx_price');
        $tbl->addIndex(['link','title'], 'idx_texts');

        //stocks
        $tbl = $schema->createTable('stocks');
        $tbl->addColumn('ean', 'integer', ['notnull'=>true]);
        $tbl->addColumn('tag_price', 'decimal', ['default'=>0]);
        $tbl->addColumn('tag_reduced_price', 'decimal', ['default'=>0]);

        $tbl->setPrimaryKey(['ean']);
        $tbl->addUniqueIndex(['ean'], 'stocks_id_uindex');
        $tbl->addIndex(['ean'], 'idx_numeric');
        $tbl->addIndex(['tag_price','tag_reduced_price'], 'idx_price');
    }

    public function down(Schema $schema) : void
    {
        $schema->dropTable('products');
        $schema->dropTable('stocks');
    }
}
