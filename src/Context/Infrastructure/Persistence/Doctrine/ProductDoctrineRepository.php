<?php
namespace App\Context\Infrastructure\Persistence\Doctrine;

use App\Common\Infrastructure\Doctrine\Repository;
use App\Context\Domain\Model\Product\Product;
use App\Context\Domain\Model\Product\ProductRepository;
use App\Context\Domain\Model\Product\ProductTagEan;
use Doctrine\ORM\NonUniqueResultException;

final class ProductDoctrineRepository extends Repository implements ProductRepository
{
    /** @noinspection PhpDocMissingThrowsInspection */

    public function save(Product $product): void
    {
        $this->persist($product);
    }

    public function get(ProductTagEan $ean)
    {
        try {
            return $this->entityManager
                ->createQuery('select p from ContextProduct:Product p where p.tagEan = :ean')
                ->setParameter('ean', $ean->toInt())
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
        }
    }
}
