<?php
namespace App\Context\Infrastructure\Persistence\Doctrine;

use App\Common\Infrastructure\Doctrine\Repository;
use App\Context\Domain\Model\Stock\Stock;
use App\Context\Domain\Model\Stock\StockRepository;
use App\Context\Domain\Model\Stock\StockTagEan;
use Doctrine\ORM\NonUniqueResultException;

final class StockDoctrineRepository extends Repository implements StockRepository
{
    /** @noinspection PhpDocMissingThrowsInspection */

    public function save(Stock $stock): void
    {
        $this->persist($stock);
    }

    public function get(StockTagEan $ean)
    {
        try {
            return $this->entityManager
                ->createQuery('select s from ContextStock:Stock s where s.tagEan = :ean')
                ->setParameter('ean', $ean->toInt())
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
        }
    }
}
