<?php
namespace App\Context\Infrastructure\Persistence\Doctrine\Type;

use App\Context\Domain\Model\Stock\StockTagEan;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\IntegerType;

class StockTagEanType extends IntegerType
{
    const NAME = 'stock_ean';

    public function getName()
    {
        return static::NAME;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return StockTagEan::fromInt($value);
    }

    /** @var StockTagEan $value */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value->toInt();
    }
}