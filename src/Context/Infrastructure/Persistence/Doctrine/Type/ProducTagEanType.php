<?php
namespace App\Context\Infrastructure\Persistence\Doctrine\Type;

use App\Context\Domain\Model\Product\ProductTagEan;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\IntegerType;

class ProducTagEanType extends IntegerType
{
    const NAME = 'product_ean';

    public function getName()
    {
        return static::NAME;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return ProductTagEan::fromInt($value);
    }

    /** @var ProductTagEan $value */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value->toInt();
    }
}