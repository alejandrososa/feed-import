<?php

namespace App\Context\Infrastructure\Feed;

interface FeedType
{
    const TYPE = 'feed';

    const FEED_PRODUCT = 'product';
    const FEED_STOCK = 'stock';
}