<?php
/**
 * Ofertaski, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2020, 29/06/2020 05:50
 */

namespace App\Context\Infrastructure\Feed;

use App\Common\Infrastructure\FeedBuilder\FeedBuilderStrategy;
use App\Context\Domain\Model\Product\Product;
use App\Context\Domain\Model\Product\ProductDescription;
use App\Context\Domain\Model\Product\ProductLink;
use App\Context\Domain\Model\Product\ProductTagEan;
use App\Context\Domain\Model\Product\ProductTagId;
use App\Context\Domain\Model\Product\ProductTagImageLink;
use App\Context\Domain\Model\Product\ProductTagPrice;
use App\Context\Domain\Model\Product\ProductTagReducedPrice;
use App\Context\Domain\Model\Product\ProductTitle;
use Zend\Feed\Reader\Feed\FeedInterface;

class FeedProductStrategy implements FeedBuilderStrategy, FeedType
{
    public function match(string $type): bool
    {
        return self::FEED_PRODUCT == $type;
    }

    public function build(?FeedInterface $feed = null): ?\Generator
    {
        $namespace = $feed->getElement()->getAttributeNode('xmlns:g')->nodeValue;

        foreach ($feed as $i => $item) {
            /** @var \DOMXPath $dom */
            $dom = $item->getXpath();
            $dom->registerNamespace('g', $namespace);

            yield Product::create(
                ProductTitle::fromString($this->getNodeValue('//item//title', $i, $dom)),
                ProductLink::fromString($this->getNodeValue('//item//link', $i, $dom)),
                ProductDescription::fromString($this->getNodeValue('//item//description', $i, $dom)),
                ProductTagImageLink::fromString($this->getNodeValue('//g:image_link', $i, $dom)),
                ProductTagPrice::fromInt($this->getNodeValue('//g:price', $i, $dom)),
                ProductTagReducedPrice::fromInt($this->getNodeValue('//g:reduced_price', $i, $dom)),
                ProductTagEan::fromInt($this->getNodeValue('//g:ean', $i, $dom)),
                ProductTagId::fromInt($this->getNodeValue('//g:id', $i, $dom))
            );
        }
    }

    private function getNodeValue(string $expression, int $i, \DOMXPath $dom)
    {
        return $dom->query($expression)[$i]->nodeValue ?? null;
    }
}