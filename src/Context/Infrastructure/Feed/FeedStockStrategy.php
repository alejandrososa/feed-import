<?php
/**
 * Ofertaski, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2020, 29/06/2020 05:58
 */

namespace App\Context\Infrastructure\Feed;

use App\Common\Infrastructure\FeedBuilder\FeedBuilderStrategy;
use App\Context\Domain\Model\Stock\Stock;
use App\Context\Domain\Model\Stock\StockTagEan;
use App\Context\Domain\Model\Stock\StockTagPrice;
use App\Context\Domain\Model\Stock\StockTagReducedPrice;
use Zend\Feed\Reader\Feed\FeedInterface;

class FeedStockStrategy implements FeedBuilderStrategy, FeedType
{
    public function match(string $type): bool
    {
        return self::FEED_STOCK == $type;
    }

    public function build(?FeedInterface $feed = null): ?\Generator
    {
        $namespace = $feed->getElement()->getAttributeNode('xmlns:g')->nodeValue;

        foreach ($feed as $i => $item) {
            /** @var \DOMXPath $dom */
            $dom = $item->getXpath();
            $dom->registerNamespace('g', $namespace);

            yield Stock::create(
                StockTagEan::fromInt($this->getNodeValue('//g:ean', $i, $dom)),
                StockTagPrice::fromInt($this->getNodeValue('//g:price', $i, $dom)),
                StockTagReducedPrice::fromInt($this->getNodeValue('//g:reduced_price', $i, $dom))
            );
        }
    }

    private function getNodeValue(string $expression, int $i, \DOMXPath $dom)
    {
        return $dom->query($expression)[$i]->nodeValue ?? null;
    }
}