<?php
namespace App\Context\Infrastructure\Feed;

use App\Common\Infrastructure\FeedBuilder\FeedChain;
use App\Common\Infrastructure\FeedBuilder\GeneralFeedFactory;

class FeedFactory extends GeneralFeedFactory
{
    public function __construct(FeedChain $chain)
    {
        $strategies = $chain->get(FeedType::TYPE);
        parent::__construct($strategies);
    }
}