<?php

namespace App\Context\Ui\Console\Command;

use App\Context\Application\Command\Product\CreateProductBatch;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class ConsumeProductsFeedCommand extends Command
{
    protected static $defaultName = 'app:consume-feed-products';

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var MessageBusInterface
     */
    private $bus;

    public function __construct(MessageBusInterface $bus, ContainerInterface $container)
    {
        $this->container = $container;
        $this->bus = $bus;

        $name = null;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Consume feed from xml and save it to database')
            ->addArgument('url', InputArgument::OPTIONAL, 'Url of feed')
            ->addOption('demo', null, InputOption::VALUE_NONE, 'Y or N. Option for demo local');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $urlResource = $input->getArgument('url');
        $demoOption = $input->getOption('demo');

        if ($demoOption) {
            $urlResource = $this->container->getParameter('kernel.project_dir');
            $urlResource .= '/config/feeds/products.xml';
        }

        if (empty($urlResource)) {
            $io->error('Url of feed is empty.');
            return Command::FAILURE;
        }

        $io->note(sprintf('You passed an argument [%s] as resource feed.', $urlResource));

        try {
            $this->bus->dispatch(CreateProductBatch::fromResource($urlResource));
            $io->success('Products successfully imported.');
            $output->writeln(sprintf('<info>Products successfully imported</info>'));
            return Command::SUCCESS;
        } catch (\Exception $e) {
            $io->error($e->getMessage());
            $output->writeln(
                sprintf('<info>Skip. Error trying bulk products. %s</info>', $e->getMessage())
            );
            return Command::FAILURE;
        }

    }
}
