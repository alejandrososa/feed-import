<?php
namespace App\Context\Ui\Web\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class PostStockController
 * @package App\Context\Ui\Web\Controller
 */
class HomeController extends AbstractController
{
    /**
     * @var MessageBusInterface
     */
    private $bus;

    /**
     * @var ContainerInterface
     */
    private $service;

    /**
     * PostStockController constructor.
     * @param MessageBusInterface $bus
     * @param ContainerInterface  $service
     */
    public function __construct(MessageBusInterface $bus, ContainerInterface $service)
    {
        $this->bus = $bus;
        $this->service = $service;
    }

    public function __invoke(Request $request)
    {
        $version = Kernel::VERSION;
        $urlFeed = \DIRECTORY_SEPARATOR;
        $docVersion = substr(Kernel::VERSION, 0, 3);

        ob_start();
        include \dirname(__DIR__).'/Views/welcome.html.php';

        return new Response(ob_get_clean(), Response::HTTP_OK);
    }
}
