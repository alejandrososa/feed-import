<?php
namespace App\Context\Ui\Web\Controller;

use App\Context\Application\Command\Stock\CreateStockBatch;
use App\Context\Ui\Web\Form\ResourceType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class PostStockController
 * @package App\Context\Ui\Web\Controller
 */
class PostStockController extends AbstractController
{
    /**
     * @var MessageBusInterface
     */
    private $bus;

    /**
     * @var ContainerInterface
     */
    private $service;

    /**
     * PostStockController constructor.
     * @param MessageBusInterface $bus
     * @param ContainerInterface  $service
     */
    public function __construct(MessageBusInterface $bus, ContainerInterface $service)
    {
        $this->bus = $bus;
        $this->service = $service;
    }

    public function __invoke(Request $request)
    {
        $form = $this->createForm(ResourceType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->bus->dispatch(
                    CreateStockBatch::fromResource($form->get('resource')->getData())
                );
                $this->addFlash("success", "Stocks successfully imported");
            } catch (\Exception $e) {
                $this->addFlash("danger", $e->getMessage());
            }

            return $this->redirect('stocks');
        }

        return $this->render('stocks/stocks.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
