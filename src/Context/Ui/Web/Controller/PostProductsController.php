<?php

namespace App\Context\Ui\Web\Controller;

use App\Context\Application\Command\Product\CreateProductBatch;
use App\Context\Ui\Web\Form\ResourceType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class PagesController
 * @package App\Context\Infrastructure\Controller
 */
class PostProductsController extends AbstractController
{
    /**
     * @var MessageBusInterface
     */
    private $bus;

    /**
     * @var ContainerInterface
     */
    private $service;

    /**
     * ProductController constructor.
     * @param MessageBusInterface $bus
     * @param ContainerInterface $service
     */
    public function __construct(MessageBusInterface $bus, ContainerInterface $service)
    {
        $this->bus = $bus;
        $this->service = $service;
    }

    public function __invoke(Request $request)
    {
        $form = $this->createForm(ResourceType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->bus->dispatch(
                    CreateProductBatch::fromResource($form->get('resource')->getData())
                );
                $this->addFlash("success", "Products successfully imported");
            } catch (\Exception $e) {
                $this->addFlash("danger", $e->getMessage());
            }

            return $this->redirect('products');
        }

        return $this->render('products/products.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
