<?php
namespace App\Context\Domain\Model\Stock;

use App\Common\Domain\Model\ValueObjects\ValueObject;
use App\Context\Domain\Exception\InvalidValue;
use Assert\Assertion;
use Assert\AssertionFailedException;

final class StockTagPrice implements ValueObject
{
    private $price;

    public function __construct(?int $price)
    {
        $this->guard($price);
        $this->price = $price;
    }

    public static function fromInt(?int $price): self
    {
        return new self($price);
    }

    public function toInt(): int
    {
        return (int) $this->price;
    }

    public function __toString(): string
    {
        return $this->price;
    }

    public function equals(ValueObject $object): bool
    {
        /** @var self $object */
        return get_class($this) === get_class($object)
            && $this->price === $object->toInt();
    }

    private function guard(?int $value): void
    {
        try {
            Assertion::numeric($value, 'The stock price is wrong, only numeric');
        } catch (AssertionFailedException $e) {
            throw InvalidValue::reason($e->getMessage(), $value);
        }
    }
}