<?php

namespace App\Context\Domain\Model\Stock;

use App\Common\Domain\Model\ValueObjects\Entity;

class Stock implements Entity
{
    /**
     * @var StockTagEan
     */
    private $tagEan;

    /**
     * @var StockTagPrice
     */
    private $tagPrice;

    /**
     * @var StockTagReducedPrice
     */
    private $tagReducedPrice;

    //region Properties model
    public function tagEan(): StockTagEan
    {
        return $this->tagEan;
    }

    public function tagPrice(): StockTagPrice
    {
        return $this->tagPrice;
    }

    public function tagReducedPrice(): StockTagReducedPrice
    {
        return $this->tagReducedPrice;
    }
    //endregion

    //region Methods
    private function __construct(
        StockTagEan $tagEan,
        StockTagPrice $tagPrice,
        StockTagReducedPrice $tagReducedPrice
    ) {
        $this->tagEan = $tagEan;
        $this->tagPrice = $tagPrice;
        $this->tagReducedPrice = $tagReducedPrice;
    }

    public static function create(
        StockTagEan $tagEan,
        StockTagPrice $tagPrice,
        StockTagReducedPrice $tagReducedPrice
    ) {
        return new self($tagEan, $tagPrice, $tagReducedPrice);
    }

    public function update(StockTagPrice $tagPrice, StockTagReducedPrice $tagReducedPrice)
    {
        $this->tagPrice = $tagPrice;
        $this->tagReducedPrice = $tagReducedPrice;
    }

    public function sameIdentityAs(Entity $other): bool
    {
        /* @var self $other */
        return get_class($this) === get_class($other)
            && $this->tagEan->equals($other->tagEan);
    }
    //endregion
}
