<?php
namespace App\Context\Domain\Model\Stock;

use App\Common\Domain\Model\ValueObjects\ValueObject;
use App\Context\Domain\Exception\InvalidValue;
use Assert\Assertion;
use Assert\AssertionFailedException;

final class StockTagEan implements ValueObject
{
    private $ean;

    public function __construct(?int $ean)
    {
        $this->guard($ean);
        $this->ean = $ean;
    }

    public static function fromInt(?int $ean): self
    {
        return new self($ean);
    }

    public function toInt(): int
    {
        return (int) $this->ean;
    }

    public function __toString(): string
    {
        return $this->ean;
    }

    public function equals(ValueObject $object): bool
    {
        /** @var self $object */
        return get_class($this) === get_class($object)
            && $this->ean === $object->toInt();
    }

    private function guard(?int $value): void
    {
        try {
            Assertion::numeric($value, 'The stock ean is wrong, only numeric');
        } catch (AssertionFailedException $e) {
            throw InvalidValue::reason($e->getMessage(), $value);
        }
    }
}