<?php
namespace App\Context\Domain\Model\Stock;

interface StockRepository
{
    public function get(StockTagEan $ean);

    public function save(Stock $stock): void;
}