<?php
namespace App\Context\Domain\Model\Product;

interface ProductRepository
{
    public function get(ProductTagEan $ean);

    public function save(Product $product): void;
}