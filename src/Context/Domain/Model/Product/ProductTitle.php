<?php
namespace App\Context\Domain\Model\Product;

use App\Common\Domain\Model\ValueObjects\ValueObject;
use App\Context\Domain\Exception\InvalidValue;
use Assert\Assertion;
use Assert\AssertionFailedException;

final class ProductTitle implements ValueObject
{
    private const MIN_LENGTH = 3;
    private const MAX_LENGTH = 144;

    private $title;

    public function __construct(string $title)
    {
        $this->guard($title);
        $this->title = $title;
    }

    public static function fromString(string $title): self
    {
        return new self($title);
    }

    public function toString(): string
    {
        return $this->title;
    }

    public function __toString(): string
    {
        return $this->title;
    }

    public function equals(ValueObject $object): bool
    {
        /** @var self $object */
        return get_class($this) === get_class($object)
            && $this->title === $object->toString();
    }

    private function guard(string $value): void
    {
        try {
            Assertion::notEmpty($value, 'The product title is empty');
            Assertion::minLength($value, self::MIN_LENGTH, 'The title is very short');
            Assertion::maxLength($value, self::MAX_LENGTH, 'The title is very long');
        } catch (AssertionFailedException $e) {
            throw InvalidValue::reason($e->getMessage(), $value);
        }
    }
}