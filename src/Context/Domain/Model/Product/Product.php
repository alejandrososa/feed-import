<?php
namespace App\Context\Domain\Model\Product;

use App\Common\Domain\Model\ValueObjects\Entity;

class Product implements Entity
{
    /**
     * @var ProductTitle
     */
    private $title;

    /**
     * @var ProductLink
     */
    private $link;

    /**
     * @var ProductDescription
     */
    private $description;

    /**
     * @var ProductTagImageLink
     */
    private $tagImageLink;

    /**
     * @var ProductTagPrice
     */
    private $tagPrice;

    /**
     * @var ProductTagReducedPrice
     */
    private $tagReducedPrice;

    /**
     * @var ProductTagEan
     */
    private $tagEan;

    /**
     * @var ProductTagId
     */
    private $tagId;

    //region Properties model
    public function tagEan(): ProductTagEan
    {
        return $this->tagEan;
    }

    public function tagId(): ProductTagId
    {
        return $this->tagId;
    }

    public function tagImageLink(): ProductTagImageLink
    {
        return $this->tagImageLink;
    }

    public function tagPrice(): ProductTagPrice
    {
        return $this->tagPrice;
    }

    public function tagReducedPrice(): ProductTagReducedPrice
    {
        return $this->tagReducedPrice;
    }

    public function title(): ProductTitle
    {
        return $this->title;
    }

    public function link(): ProductLink
    {
        return $this->link;
    }

    public function description(): ProductDescription
    {
        return $this->description;
    }
    //endregion

    //region Methods
    private function __construct(
        ProductTitle $title,
        ProductLink $link,
        ProductDescription $description,
        ProductTagImageLink $tagImageLink,
        ProductTagPrice $tagPrice,
        ProductTagReducedPrice $tagReducedPrice,
        ProductTagEan $tagEan,
        ProductTagId $tagId
    ) {
        $this->title = $title;
        $this->link = $link;
        $this->description = $description;
        $this->tagImageLink = $tagImageLink;
        $this->tagPrice = $tagPrice;
        $this->tagReducedPrice = $tagReducedPrice;
        $this->tagEan = $tagEan;
        $this->tagId = $tagId;
    }

    public static function create(
        ProductTitle $title,
        ProductLink $link,
        ProductDescription $description,
        ProductTagImageLink $tagImageLink,
        ProductTagPrice $tagPrice,
        ProductTagReducedPrice $tagReducedPrice,
        ProductTagEan $tagEan,
        ProductTagId $tagId
    ) {
        return new self(
            $title,
            $link,
            $description,
            $tagImageLink,
            $tagPrice,
            $tagReducedPrice,
            $tagEan,
            $tagId
        );
    }

    public function update(
        ProductTitle $title,
        ProductLink $link,
        ProductDescription $description,
        ProductTagImageLink $tagImageLink,
        ProductTagPrice $tagPrice,
        ProductTagReducedPrice $tagReducedPrice,
        ProductTagId $tagId
    ) {
        $this->title = $title;
        $this->link = $link;
        $this->description = $description;
        $this->tagImageLink = $tagImageLink;
        $this->tagPrice = $tagPrice;
        $this->tagReducedPrice = $tagReducedPrice;
        $this->tagId = $tagId;
    }

    public function sameIdentityAs(Entity $other): bool
    {
        /* @var self $other */
        return get_class($this) === get_class($other)
            && $this->tagEan->equals($other->tagEan);
    }
    //endregion
}
