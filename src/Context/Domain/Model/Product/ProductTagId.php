<?php
namespace App\Context\Domain\Model\Product;

use App\Common\Domain\Model\ValueObjects\ValueObject;
use App\Context\Domain\Exception\InvalidValue;
use Assert\Assertion;
use Assert\AssertionFailedException;

final class ProductTagId implements ValueObject
{
    private $id;

    public function __construct(int $id)
    {
        $this->guard($id);
        $this->id = $id;
    }

    public static function fromInt(int $id): self
    {
        return new self($id);
    }

    public function toInt(): int
    {
        return (int) $this->id;
    }

    public function __toString(): string
    {
        return $this->id;
    }

    public function equals(ValueObject $object): bool
    {
        /** @var self $object */
        return get_class($this) === get_class($object)
            && $this->id === $object->toInt();
    }

    private function guard(int $value): void
    {
        try {
            Assertion::numeric($value, 'The product id is wrong, only numeric');
        } catch (AssertionFailedException $e) {
            throw InvalidValue::reason($e->getMessage(), $value);
        }
    }
}