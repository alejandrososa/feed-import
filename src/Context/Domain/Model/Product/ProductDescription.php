<?php
namespace App\Context\Domain\Model\Product;

use App\Common\Domain\Model\ValueObjects\ValueObject;
use App\Context\Domain\Exception\InvalidValue;
use Assert\Assertion;
use Assert\AssertionFailedException;

final class ProductDescription implements ValueObject
{
    private const MIN_LENGTH = 3;
    private const MAX_LENGTH = 1024;

    private $description;

    public function __construct(string $description)
    {
        $this->guard($description);
        $this->description = $description;
    }

    public static function fromString(string $description): self
    {
        return new self($description);
    }

    public function toString(): string
    {
        return $this->description;
    }

    public function __toString(): string
    {
        return $this->description;
    }

    public function equals(ValueObject $object): bool
    {
        /** @var self $object */
        return get_class($this) === get_class($object)
            && $this->description === $object->toString();
    }

    private function guard(string $value): void
    {
        try {
            Assertion::notEmpty($value, 'The product description is empty');
            Assertion::minLength($value, self::MIN_LENGTH, 'The product description is very short');
            Assertion::maxLength($value, self::MAX_LENGTH, 'The product description is very long');
        } catch (AssertionFailedException $e) {
            throw InvalidValue::reason($e->getMessage(), $value);
        }
    }
}