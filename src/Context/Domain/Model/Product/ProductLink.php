<?php
namespace App\Context\Domain\Model\Product;

use App\Common\Domain\Model\ValueObjects\ValueObject;
use App\Context\Domain\Exception\InvalidValue;
use Assert\Assertion;
use Assert\AssertionFailedException;

final class ProductLink implements ValueObject
{
    private const MIN_LENGTH = 3;
    private const MAX_LENGTH = 144;

    private $link;

    public function __construct(string $link)
    {
        $this->guard($link);
        $this->link = $link;
    }

    public static function fromString(string $link): self
    {
        return new self($link);
    }

    public function toString(): string
    {
        return $this->link;
    }

    public function __toString(): string
    {
        return $this->link;
    }

    public function equals(ValueObject $object): bool
    {
        /** @var self $object */
        return get_class($this) === get_class($object)
            && $this->link === $object->toString();
    }

    private function guard(string $value): void
    {
        try {
            Assertion::notEmpty($value, 'The product link is empty');
            Assertion::minLength($value, self::MIN_LENGTH, 'The link is very short');
            Assertion::maxLength($value, self::MAX_LENGTH, 'The link is very long');
        } catch (AssertionFailedException $e) {
            throw InvalidValue::reason($e->getMessage(), $value);
        }
    }
}