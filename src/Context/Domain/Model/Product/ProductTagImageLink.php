<?php
namespace App\Context\Domain\Model\Product;

use App\Common\Domain\Model\ValueObjects\ValueObject;
use App\Context\Domain\Exception\InvalidValue;
use Assert\Assertion;
use Assert\AssertionFailedException;

final class ProductTagImageLink implements ValueObject
{
    private $imageLink;

    public function __construct(string $imageLink)
    {
        $this->guard($imageLink);
        $this->imageLink = $imageLink;
    }

    public static function fromString(string $imageLink): self
    {
        return new self($imageLink);
    }

    public function toString(): string
    {
        return $this->imageLink;
    }

    public function __toString(): string
    {
        return $this->imageLink;
    }

    public function equals(ValueObject $object): bool
    {
        /** @var self $object */
        return get_class($this) === get_class($object)
            && $this->imageLink === $object->toString();
    }

    private function guard(string $value): void
    {
        try {
            Assertion::notEmpty($value, 'The product image link is empty');
            Assertion::url($value, 'The product image link is not a valid url');
        } catch (AssertionFailedException $e) {
            throw InvalidValue::reason($e->getMessage(), $value);
        }
    }
}