<?php
namespace App\Context\Domain\Exception;

final class InvalidValue extends \InvalidArgumentException
{
    public static function reason(string $msg, $value): self
    {
        return new self(sprintf("Upss, '%s' %s", $value, $msg));
    }
}