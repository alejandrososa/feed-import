<?php
namespace App\Context\Application\Exception;

final class WrongResourceException extends \Exception
{
    public static function reason(string $msg, $value): self
    {
        return new self(sprintf("Upss, '%s' %s", $value, $msg));
    }
}