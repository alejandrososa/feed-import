<?php
namespace App\Context\Application\Command\Stock;

class CreateStockBatch
{
    private $resource;

    private function __construct(?string $resource)
    {
        $this->resource = $resource;
    }

    public static function fromResource(?string $resource): self
    {
        return new self($resource);
    }

    public function resource(): ?string
    {
        return $this->resource;
    }
}