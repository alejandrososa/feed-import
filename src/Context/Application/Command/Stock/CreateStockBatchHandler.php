<?php
namespace App\Context\Application\Command\Stock;

use App\Common\Infrastructure\FeedBuilder\Context;
use App\Context\Application\Exception\WrongResourceException;
use App\Context\Domain\Model\Stock\Stock;
use App\Context\Domain\Model\Stock\StockRepository;
use App\Context\Domain\Model\Stock\StockTagEan;
use App\Context\Infrastructure\Feed\FeedType;
use App\Context\Infrastructure\Persistence\Doctrine\StockDoctrineRepository;
use Eko\FeedBundle\Feed\Reader;
use Eko\FeedBundle\Hydrator\DefaultHydrator;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class CreateStockBatchHandler implements MessageHandlerInterface
{
    /**
     * @var StockDoctrineRepository
     */
    private $repository;

    /**
     * @var Reader
     */
    private $reader;
    /**
     * @var Context
     */
    private $feedContext;

    public function __construct(
        StockRepository $repository,
        Reader $reader,
        Context $feedContext
    ) {
        $this->repository = $repository;
        $this->reader = $reader;
        $this->feedContext = $feedContext;
    }

    /**
     * @param CreateStockBatch $command
     * @throws WrongResourceException
     */
    public function __invoke(CreateStockBatch $command)
    {
        $this->guardResourceIsNotEmpty($command->resource());

        $stocks = $this->getStockFromResource($command);
        $this->saveInBatches($stocks);
    }

    /**
     * Check if resource url or document xml is not empty
     * @param string|null $resource
     * @throws WrongResourceException
     */
    private function guardResourceIsNotEmpty(?string $resource)
    {
        if (empty($resource)) {
            throw WrongResourceException::reason('resource is empty, current:', $resource);
        }
    }

    /**
     * @param CreateStockBatch $command
     * @return \Generator
     * @throws WrongResourceException
     */
    private function getStockFromResource(CreateStockBatch $command): \Generator
    {
        try {
            $reader = $this->reader;
            $reader->setHydrator(new DefaultHydrator());
            $feed = $reader->load($command->resource())->get();

            return $this->feedContext
                ->setType(FeedType::FEED_STOCK)
                ->build($feed);

        } catch (\Exception | \TypeError $e) {
            throw WrongResourceException::reason('resource is not valid', $command->resource());
        }
    }

    private function saveInBatches(?\Generator $stocks)
    {
        foreach ($stocks as $stock) {
            /** @var Stock $stock $_stock  */
            if ($_stock = $this->guardExistStockInDB($stock->tagEan())) {
                $_stock->update($stock->tagPrice(), $stock->tagReducedPrice());
                $this->repository->save($_stock);
            } else {
                $this->repository->save($stock);
            }
        }
    }

    private function guardExistStockInDB(StockTagEan $tagEan): ?Stock
    {
        return $this->repository->get($tagEan);
    }
}
