<?php
namespace App\Context\Application\Command\Product;

class CreateProductBatch
{
    private $resource;

    private function __construct(?string $resource)
    {
        $this->resource = $resource;
    }

    public static function fromResource(?string $resource): self
    {
        return new self($resource);
    }

    public function resource(): ?string
    {
        return $this->resource;
    }
}