<?php

namespace App\Context\Application\Command\Product;

use App\Common\Infrastructure\FeedBuilder\Context;
use App\Common\Infrastructure\FeedBuilder\FeedBuilderContext;
use App\Context\Application\Exception\WrongResourceException;
use App\Context\Domain\Model\Product\Product;
use App\Context\Domain\Model\Product\ProductRepository;
use App\Context\Domain\Model\Product\ProductTagEan;
use App\Context\Infrastructure\Feed\FeedType;
use App\Context\Infrastructure\Persistence\Doctrine\ProductDoctrineRepository;
use Eko\FeedBundle\Feed\Reader;
use Eko\FeedBundle\Hydrator\DefaultHydrator;
use Symfony\Component\Messenger\Handler\MessageSubscriberInterface;

class CreateProductBatchHandler implements MessageSubscriberInterface
{
    /**
     * @var ProductDoctrineRepository
     */
    private $repository;

    /**
     * @var Reader
     */
    private $reader;
    /**
     * @var Context
     */
    private $feedContext;

    public function __construct(
        ProductRepository $repository,
        Reader $reader,
        FeedBuilderContext $feedContext
    ) {
        $this->repository = $repository;
        $this->reader = $reader;
        $this->feedContext = $feedContext;
    }

    public static function getHandledMessages(): iterable
    {
        yield CreateProductBatch::class;
    }

    /**
     * @param CreateProductBatch $command
     * @throws WrongResourceException
     */
    public function __invoke(CreateProductBatch $command)
    {
        $this->guardResourceIsNotEmpty($command->resource());

        $products = $this->getProductFromResource($command);
        $this->saveInBatches($products);
    }

    /**
     * Check if resource url or document xml is not empty
     * @param string|null $resource
     * @throws WrongResourceException
     */
    private function guardResourceIsNotEmpty(?string $resource)
    {
        if (empty($resource)) {
            throw WrongResourceException::reason('resource is empty, current:', $resource);
        }
    }

    /**
     * @param CreateProductBatch $command
     * @return \Generator
     * @throws WrongResourceException
     */
    private function getProductFromResource(CreateProductBatch $command): \Generator
    {
        try {
            $reader = $this->reader;
            $reader->setHydrator(new DefaultHydrator());
            $feed = $reader->load($command->resource())->get();

            return $this->feedContext
                ->setType(FeedType::FEED_PRODUCT)
                ->build($feed);

        } catch (\Exception | \TypeError $e) {
            throw WrongResourceException::reason('resource is not valid', $command->resource());
        }
    }

    private function saveInBatches(?\Generator $stocks)
    {
        foreach ($stocks as $product) {
            /** @var Product $product $_product */
            if ($_product = $this->guardExistProductInDB($product->tagEan())) {
                $_product->update(
                    $product->title(),
                    $product->link(),
                    $product->description(),
                    $product->tagImageLink(),
                    $product->tagPrice(),
                    $product->tagReducedPrice(),
                    $product->tagId()
                );
                $this->repository->save($_product);
            } else {
                $this->repository->save($product);
            }
        }
    }

    private function guardExistProductInDB(ProductTagEan $tagEan): ?Product
    {
        return $this->repository->get($tagEan);
    }
}
