<?php
namespace App\Common\Infrastructure\Doctrine;

use App\Common\Domain\Model\ValueObjects\Entity;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

abstract class Repository
{
    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    protected function entityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    protected function persist(Entity $entity): void
    {
        $this->entityManager()->persist($entity);
        $this->entityManager()->flush();
    }

    protected function remove(Entity $entity): void
    {
        $this->entityManager()->remove($entity);
        $this->entityManager()->flush();
    }

    protected function persister(): callable
    {
        return function (Entity $entity): void {
            $this->persist($entity);
        };
    }

    protected function remover(): callable
    {
        return function (Entity $entity): void {
            $this->remove($entity);
        };
    }

    /**
     * @param $entityClass
     * @return \Doctrine\Common\Persistence\ObjectRepository|EntityRepository
     */
    protected function repository($entityClass)
    {
        return $this->entityManager->getRepository($entityClass);
    }

    protected function queryBuilder(): QueryBuilder
    {
        return $this->entityManager->createQueryBuilder();
    }
}