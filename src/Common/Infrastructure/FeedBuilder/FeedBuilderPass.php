<?php
namespace App\Common\Infrastructure\FeedBuilder;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class FeedBuilderPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->has(FeedChain::class)) {
            return;
        }

        $definition = $container->findDefinition(FeedChain::class);

        $taggedServices = $container->findTaggedServiceIds('app.feed_builder');

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $attributes) {
                $definition->addMethodCall('add', [
                    new Reference($id),
                    $attributes['alias']
                ]);
            }
        }
    }
}
