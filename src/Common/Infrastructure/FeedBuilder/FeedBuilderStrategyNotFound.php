<?php
namespace App\Common\Infrastructure\FeedBuilder;

class FeedBuilderStrategyNotFound extends \Exception
{
    public function __construct()
    {
        parent::__construct('Feed builder strategy not found');
    }
}