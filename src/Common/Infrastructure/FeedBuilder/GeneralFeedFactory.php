<?php
namespace App\Common\Infrastructure\FeedBuilder;

abstract class GeneralFeedFactory implements FeedFactory
{
    /**
     * @var \Generator
     */
    protected $strategy;

    public function __construct(?\Generator $strategy)
    {
        $this->strategy = $strategy;
    }

    public function strategies(): ?\Generator
    {
        if (!$this->strategy->valid()) {
            return null;
        }

        foreach ($this->strategy as $strategy) {
            yield $strategy;
        }
    }
}
