<?php
namespace App\Common\Infrastructure\FeedBuilder;

use Generator;

class FeedChain
{
    /**
     * @var array
     */
    protected $strategies = [];

    public function add(FeedBuilderStrategy $strategy, string $alias): void
    {
        $this->strategies[][$alias] = $strategy;
    }

    public function get(string $alias): ?Generator
    {
        foreach ($this->strategies as $idx => $strategies) {
            if (array_key_exists($alias, $strategies)) {
                foreach ($strategies as $strategy) {
                    yield $strategy;
                }
            }
        }
    }
}
