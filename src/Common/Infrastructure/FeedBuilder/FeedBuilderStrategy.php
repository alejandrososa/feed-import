<?php
namespace App\Common\Infrastructure\FeedBuilder;

use Zend\Feed\Reader\Feed\FeedInterface;

interface FeedBuilderStrategy
{
    public function match(string $type): bool;

    public function build(FeedInterface $feed = null): ?\Generator;
}
