<?php
namespace App\Common\Infrastructure\FeedBuilder;

use Generator;

interface FeedFactory
{
    public function strategies(): ?Generator;
}
