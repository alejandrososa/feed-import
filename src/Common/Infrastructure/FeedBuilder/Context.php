<?php
/**
 * Ofertaski, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2020, 29/06/2020 06:15
 */

namespace App\Common\Infrastructure\FeedBuilder;

use Zend\Feed\Reader\Feed\FeedInterface;

interface Context
{
    public function setType($type): Context;

    public function build(FeedInterface $feed = null): ?\Generator;
}