<?php
namespace App\Common\Infrastructure\FeedBuilder;

use Zend\Feed\Reader\Feed\FeedInterface;

class FeedBuilderContext implements Context
{
    private $factory;
    private $type;

    public function __construct(FeedFactory $factory)
    {
        $this->factory = $factory;
    }

    public function setType($type): Context
    {
        $this->type = $type;
        return $this;
    }

    public function build(FeedInterface $feed = null): ?\Generator
    {
        if (empty($this->type)) {
            throw new FeedBuilderStrategyNotFound();
        }

        foreach ($this->factory->strategies() as $strategy) {
            /** @var FeedBuilderStrategy $strategy */
            if ($strategy->match($this->type)) {
                return $strategy->build($feed);
            }
        }
    }
}
