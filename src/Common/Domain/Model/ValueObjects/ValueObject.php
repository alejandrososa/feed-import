<?php
namespace App\Common\Domain\Model\ValueObjects;

interface ValueObject
{
    public function equals(ValueObject $object): bool;
}
