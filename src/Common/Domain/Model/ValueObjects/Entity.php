<?php
namespace App\Common\Domain\Model\ValueObjects;

interface Entity
{
    public function sameIdentityAs(Entity $other): bool;
}