#!/bin/bash

deployBackend(){
    currentPath=$(pwd)
    composer install
    cd $currentPath
    php bin/console doctrine:database:create --if-not-exists
    echo 'y' | php bin/console doctrine:migrations:migrate
    php -S 127.0.0.1:8003 -t public/
}

executeTests(){
    php bin/phpunit
}

deployBackend
executeTests

echo "----------------"
echo "DEPLOY FINISHED \n"
echo "Go to http://127.0.0.1:8003 \n"
echo "Enjoy :) \n"
