# Symfony Feed Test 

Docker symfony gives you everything you need for developing Symfony application. This complete stack run with docker and [docker-compose](https://docs.docker.com/compose/).

## Installation fast

First of all, the first thing you should do is configure the ip for the mysql database, edit the following file:

    $ cp .env.dist .env && nano .env

Edit with your database vars the following line into `.env` and `.env.test` too:

    #DOCTRINE
    DATABASE_URL=mysql://USER:PASSWORD@127.0.0.1:3306/store
    
Create phpunit.xml file 

    cp phpunit.xml.dist phpunit.xml
    
Setting vars database to tests into `phpunit.xml`:

    <env name="DATABASE_URL" value="mysql://db_user:db_password@127.0.0.1:3306/db_name"/>

Now execute the next command in your console:

    $ sh deploy.sh


Enjoy :-)

##WEB

* Visit [127.0.0.1:8003](http://127.0.0.1:8003)

##CONSOLE

    $  php bin/console  app:consume-feed-stocks --demo
    $  php bin/console  app:consume-feed-products --demo
